#!/bin/sh
exec java -cp /opt/h2/bin/$H2_DRIVER.jar org.h2.tools.Server \
  -webAllowOthers -webPort 8082 \
  -tcpAllowOthers -tcpPort 9092 \
  -baseDir $DATA_DIR
