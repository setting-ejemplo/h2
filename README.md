# README #

## H2 ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/h2.git

### Crear imagen
* docker build -t settingejemplo/h2 .

### Crear contenedor ###
* docker run
  --detach
  --restart always
  --name h2-container
  --network setting
  --ip 172.13.1.20
  --publish 8082:8082
  --publish 9092:9092
  --volume /srv/h2/data:/opt/h2/data
  settingejemplo/h2
