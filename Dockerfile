FROM java:8

MAINTAINER ocutrin <ocutrin@setting.dev>

USER root

# Directorio de trabajo
WORKDIR /opt

# Nombre directorio base datos
ENV DATA_DIR data

# Nombre paquete zip
ENV H2_ZIP h2-2014-04-05

# Nombre del drive incluido dentro del paquete zip
ENV H2_DRIVER h2-1.3.176

# Añadimos el zip al contenedor
ADD $H2_ZIP.zip ./

# Instalacion h2
RUN unzip $H2_ZIP.zip && rm $H2_ZIP.zip && mkdir -p /$DATA_DIR

# Exponemos puertos que utilizaremos 8082 web 9092 tcp
EXPOSE 8082 9092

# Copiamos ejecutable y motamos el archivo de ejecucion
COPY start.sh /
ENTRYPOINT ["/start.sh"]
